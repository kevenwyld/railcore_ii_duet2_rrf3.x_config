# railcore_ii_duet2_RRF3.x_config

RepRap Firmware configs for the Filastruder Railcore II 3d printer kit (purchase date July 2021).  Most of the original code came from https://github.com/JohnOCFII/RailCore and was modified to work with the components in the kit.